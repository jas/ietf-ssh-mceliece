# Render markdown into an I-D
#
# Prerequisites: ruby-kramdown-rfc2629 xml2rfc GNU-Make python
#
# For pdf output, also: weasyprint
# Tested with the Debian versions of the tools

###########################################################################
# DRAFT_BASENAME is the name of your markdown file WITHOUT the .md suffix #
###########################################################################
DRAFT_BASENAME = ietf-ssh-mceliece
OUTPUT = $(DRAFT_BASENAME).txt $(DRAFT_BASENAME).html $(DRAFT_BASENAME).xml

all: $(OUTPUT)

%.xml: %.md $(wildcard test-vectors/*)
	kramdown-rfc2629 --v3 $< > $@.tmp
	mv $@.tmp $@

%.html: %.xml
	xml2rfc --v3 $< --html

%.txt: %.xml
	xml2rfc --v3 $< --text

%.pdf: %.xml
	xml2rfc --v3 $< --pdf

$(DRAFT_BASENAME).txt.diff: $(DRAFT_BASENAME).txt compare canonicalizetxt
	! ./compare > $@.tmp
	mv $@.tmp $@

# builds a docker image that can build the targets in this makefile (except pdf output)
# see kramdown-rfc2629-docker/Dockerfile for documentation, it's only three lines
docker-image:
	docker build -t kramdown-rfc2629-docker kramdown-rfc2629-docker/

# runs "make all" in a docker container that mounts this directory as a volume
# this takes the UID and GID from the current env, so the output files don't belong the root
docker-all:
	docker run --rm -i --user ${UID}:${GID} -v $(PWD):/rfc kramdown-rfc2629-docker:latest make

# drops you in a shell in the container, with this directory mounted where you can run "make" more quickly
# this takes the UID and GID from the current env, so the output files don't belong the root
docker-shell:
	docker run --rm -it --user ${UID}:${GID} -v $(PWD):/rfc kramdown-rfc2629-docker:latest bash

clean:
	-rm -rf $(OUTPUT) *.tmp $(DRAFT_BASENAME).txt.diff $(DRAFT_BASENAME).md.reflowed

check: codespell check-reflow trailing-whitespace

check-reflow:
	./reflow < $(DRAFT_BASENAME).md > $(DRAFT_BASENAME).md.reflowed
	diff -u $(DRAFT_BASENAME).md $(DRAFT_BASENAME).md.reflowed

codespell:
	codespell $(DRAFT_BASENAME).md

trailing-whitespace:
	! grep -n '[[:space:]]$$' $(DRAFT_BASENAME).md

.PHONY: clean all check codespell check-reflow
