---
title: "Secure Shell Key Exchange Method Using Hybrid Classic McEliece and X25519 with SHA-512: mceliece6688128x25519-sha512"
abbrev: "Classic McEliece for SSH"
category: std
docname: draft-josefsson-ssh-mceliece-00
ipr: trust200902
area: int
keyword: Internet-Draft
submissionType: IETF
venue:
  type: "Working Group"
  repo: "https://gitlab.com/jas/ietf-ssh-mceliece"
stand_alone: yes
pi: [toc, sortrefs, symrefs]
author:
 -
    fullname: Simon Josefsson
    email: simon@josefsson.org
normative:
  RFC4251:
  RFC4253:
  RFC5656:
  RFC8731:
informative:
  RFC6234:
  RFC7748:
  I-D.josefsson-mceliece:
  I-D.josefsson-ntruprime-ssh:
  CM-spec:
    target: https://classic.mceliece.org/mceliece-spec-20221023.pdf
    title: "Classic McEliece: conservative code-based cryptography: cryptosystem specification"
    author:
      org: Classic McEliece Team
    date: October 2022
  CM-impl:
    target: https://classic.mceliece.org/mceliece-impl-20221023.pdf
    title: "Classic McEliece: conservative code-based cryptography: guide for implementors"
    author:
      org: Classic McEliece Team
    date: October 2022
  CM-security:
    target: https://classic.mceliece.org/mceliece-security-20221023.pdf
    title: "Classic McEliece: conservative code-based cryptography: guide for security reviewers"
    author:
      org: Classic McEliece Team
    date: October 2022
  IANA-KEX:
    target: https://www.iana.org/assignments/ssh-parameters/
    title: "Secure Shell (SSH) Protocol Parameters: Key Exchange Method Names"
    author:
      org: IANA
  OpenSSH:
    target: https://www.openssh.com/
    title: "OpenSSH"
    author:
      org: OpenSSH team
  OpenSSH-McEliece-patch:
    target: https://gitlab.com/jas/openssh-portable/-/tree/jas/mceliece
    title: "GitLab branch of OpenSSH with McEliece support"
    author:
      org: OpenSSH team, Simon Josefsson

--- abstract

This document specify a hybrid key exchange method in the Secure Shell (SSH) protocol based on Classic McEliece (mceliece6688128) and X25519 with SHA-512.

--- middle

# Introduction {#introduction}

Secure Shell (SSH) {{RFC4251}} is a secure remote login protocol.
The key exchange protocol described in {{RFC4253}} supports an extensible set of methods.
{{RFC5656}} defines how elliptic curves are integrated into this extensible SSH framework, and {{RFC8731}} specify "curve25519-sha256" to support the pre-quantum elliptic-curve Diffie-Hellman X25519 function {{RFC7748}}.
In {{I-D.josefsson-ntruprime-ssh}} it is described how the post-quantum lattice-based Streamlined NTRU Prime is combined with X25519 for SSH, and we base our protocol and document on it but replace sntrup761 with mceliece6688128.

Classic McEliece {{I-D.josefsson-mceliece}} {{CM-spec}} provides a code-based Key Encapsulation Method designed to be safe even against quantum computers.
The variant "mceliece6688128" offers a balance between performance and output sizes.

To hedge against attacks on either of mceliece6688128 or X25519 a hybrid construction may be used, with the intention that the hybrid would be secure if either of the involved algorithms are flawed.

This document specify how to implement key exchange based on a hybrid between Classic McEliece mceliece6688128 and X25519 with SHA-512 {{RFC6234}} in SSH.

# Conventions and Definitions {#conventions-definitions}

{::boilerplate bcp14-tagged}

# Key Exchange Method: mceliece6688128x25519-sha512 {#mceliece6688128x25519-sha512}

The key-agreement is done by the X25519 Diffie-Hellman protocol as described in {{Section 3 (Key Exchange Methods) of RFC8731}}, and the mceliece6688128 key encapsulation method described in {{I-D.josefsson-mceliece}} {{CM-spec}}.

The key exchange procedure reuse the Elliptic Curve Diffie-Hellman (ECDH) key exchange defined in {{Sections 4 (ECDH Key Exchange) and 7.1 (ECDH Message Numbers) of RFC5656}}.
The protocol flow and the `SSH_MSG_KEX_ECDH_INIT` and `SSH_MSG_KEX_ECDH_REPLY` messages are identical, except that we use different ephemeral public values Q_C and Q_S and shared secret K as described below.

The `SSH_MSG_KEX_ECDH_INIT` value `Q_C` that holds the client's ephemeral public key MUST be constructed by concatenating the 1044992 byte public key output from the key generator of mceliece6688128 (or mceliece6688128f, see {{mceliece6688128f}}) with the 32 byte K_A = X25519(a, 9) as described in {{I-D.josefsson-mceliece}} {{CM-spec}} and {{RFC8731}}.
The Q_C value is thus 1045024 bytes.

The `SSH_MSG_KEX_ECDH_REPLY` value `Q_S` that holds the server's ephemeral public key MUST be constructed by concatenating the 208 byte ciphertext output from the key encapsulation mechanism of mceliece6688128 (or mceliece6688128f, see {{mceliece6688128f}}) with the 32 byte K_B = X25519(b, 9) as described in {{I-D.josefsson-mceliece}} {{CM-spec}} and {{RFC8731}}.
The `Q_S` value is thus 240 bytes.

Clients and servers MUST abort if the length of the received public keys `Q_C` or `Q_S` are not the expected lengths.
An abort for these purposes is defined as a disconnect (`SSH_MSG_DISCONNECT`) of the session and SHOULD use the `SSH_DISCONNECT_KEY_EXCHANGE_FAILED` reason for the message, see {{Section 11.1 (Disconnection Message) of RFC4253}}.
No further validation is required beyond what is described in {{RFC7748}}, {{RFC8731}} and {{I-D.josefsson-mceliece}} {{CM-spec}}.

The `SSH_MSG_KEX_ECDH_REPLY` signature value is computed as described in {{RFC5656}} with the following changes.
Instead of encoding the shared secret `K` as 'mpint', it MUST be encoded as 'string'.
The shared secret K value MUST be the 64-byte output octet string of the SHA-512 hash computed with the input as the 32-byte octet string key output from the key encapsulation mechanism of mceliece6688128 (or mceliece6688128f, see {{mceliece6688128f}}) concatenated with the 32-byte octet string of X25519(a, X25519(b, 9)) = X25519(b, X25519(a, 9)).

# mceliece6688128f {#mceliece6688128f}

The f and non-f versions are interoperable.
The f versions have faster key generation, while the non-f versions have simpler key generation.
For example, a key generated with mceliece6688128f can decapsulate ciphertexts that were encapsulated with mceliece6688128, and vice versa.
The secret-key sizes (and formats) are the same, the encapsulation functions are the same, and the decapsulation functions are the same.

Implementations of this protocol can chose between mceliece6688128 or mceliece6688128f, however the name of this protocol is "mceliece6688128x25519-sha512" even for implementations that use mceliece6688128f internally.

Choosing mceliece6688128 generally reduce code size and complexity (at the expense of performance), and choosing mceliece6688128f generally improve performance (at the expense of code size and complexity).

# Acknowledgments

The protocol and document is based on {{I-D.josefsson-ntruprime-ssh}}.
The authors would like to thank {{{Daniel J. Bernstein}}} for discussion and suggesting the mceliece6688128 variant.

# Implementation Status {#implementation-status}

One implementation of this protocol is available as a patch {{OpenSSH-McEliece-patch}} for OpenSSH {{OpenSSH}}, released under a BSD-style license.

# Security Considerations {#security-considerations}

The security considerations of {{RFC4251}}, {{RFC5656}}, {{RFC7748}}, {{RFC8731}} and {{I-D.josefsson-mceliece}} {{CM-spec}} {{CM-security}} {{CM-impl}} are inherited.

Classic McEliece is a KEM designed for IND-CCA2 security at a very high security level, even against quantum computers.
The algorithm has been studied by researchers for many years, and there are implementations in the public domain for a wide range of architectures.
However new cryptographic primitives should be introduced and trusted conservatively, and new research findings may be published at any time that may warrant implementation reconsiderations.
The method described here to combine X25519 with mceliece6688128, i.e., SHA-512 hashing the concatenated outputs, is also available for the same kind of cryptographic scrutiny.

The increase in communication size and computational requirements may be a concern for limited computational devices, which would then not be able to take advantage of the improved security properties offered by this work.

As discussed in the security considerations of Curve25519-sha256 {{RFC8731}}, the X25519 shared secret `K` is used bignum-encoded in that document, and this raise a potential for a hash-processing time side-channel that could leak one bit of the secret due to different length of the bignum sign pad.
This document resolve that problem by using string-encoding instead of bignum-encoding.

# IANA Considerations {#iana-considerations}

IANA is requested to add a new "Method Name" of "mceliece6688128x25519-sha512" to the "Key Exchange Method Names" registry for Secure Shell (SSH) Protocol Parameters {{IANA-KEX}} with a "reference" field to this RFC and the "OK to implement" field of "MAY".
