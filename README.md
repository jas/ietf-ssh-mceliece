# Classic McEliece for SSH: mceliece6688128x25519-sha512

This specify a key exchange method based on [Classic
McEliece](https://classic.mceliece.org/) for SSH.

The latest output can be read here:
https://jas.gitlab.io/ietf-ssh-mceliece

Submitted versions can be found here:
https://datatracker.ietf.org/doc/draft-josefsson-ssh-mceliece/

An implementation for [OpenSSH](https://www.openssh.com/) lives in the
[jas/mceliece branch on
GitHub](https://github.com/jas4711/openssh-portable/tree/jas/mceliece).
